rpi-env-sensor
=========

Ansible Role for configure a Raspberry Pi as environmental sensor with display.
The Raspberry use a sense hat to display the temperature and humidity from
a dht22, am2302 or dht11.   

Requirements
------------
* Ansible in your deployer machine
* Hardware:
  * One Raspberry Pi
  * A environmental sensor like dht22, am2302 or dht11
  * A sense hat. The temperature and humidity sensors of sensehat aren't used.
  The sensehat shield is near the Raspberry and the temperature in the sensor is
  so hight.
  * A stack gpio pin for connect the sensehat and the environment sensor

* Operative System Raspbian



Role Variables
--------------
The variables that can be passed to this role and a brief description about them are as follows. (For all variables, take a look at vars/main.yml)

    app_name: "rpi-env-sensor"     # Name of the APP, choose one.
    app_path: "/opt/{{app_name}}/" # Path where install the code
    app_owner: "pi"                # User id of the files in the operative system
    app_group: "pi"                # Group id

    username: admin               # Credentials for the Local API Rest
    # password: XXXXX in a vault file


Dependencies
------------

You can found it in the requirements.yml. Remenber that you can install the requirementes with de command:


    ansible-galaxy install -r requirements.yml

or

    ansbible-galaxy install -r requirements -p roles



Tags
---------------

  * rpi-env-sensor: To only execute this rol

 To show tags of the role rpirest:
  * https://galaxy.ansible.com/jregueirar/rpirest
  * https://github.com/jregueirar/ansible-rpirest/blob/master/README.md

Example Playbook
----------------

See test/test.yml

    ansible-playbook tests/test.yml -l rpi2 -i tests/inventory --ask-vault-pass


License
-------

GPL-v3

Author Information
------------------

* Original: Jonás Regueira Rodríguez
